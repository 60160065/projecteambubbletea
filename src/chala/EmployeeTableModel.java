/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chala;


import database.Employees;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Windows 10
 */
public class EmployeeTableModel extends AbstractTableModel {

    ArrayList<Employees> employees = new ArrayList<Employees>();
    String[] columnNames = {"id", "ชื่อ", "นามสกุล", "เบอร์โทร", "เลขบัตรประชาชน", "ตำแหน่ง", "รหัสผู้ใช้", "รหัสผ่าน"};

    @Override
    public int getRowCount() {
        return employees.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Employees lp = employees.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return lp.getEmpId();
            case 1:
                return lp.getName();
            case 2:
                return lp.getSurname();
            case 3:
                return lp.getPhone();
            case 4:
                return lp.getCardId();
            case 5:
                return lp.getType();
            case 6:
                return lp.getUsername();
            case 7:
                return lp.getPassword();
        }
        return "";
    }

    public void setData(ArrayList<Employees> employees) {
        this.employees = employees;
        fireTableDataChanged();
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }
}
