/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chala;


import database.ListProduct;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Windows 10
 */
public class OrderingTableModel extends AbstractTableModel {
    ArrayList<ListProduct> ProductList = new ArrayList<ListProduct>();
    String[] columnNames = {"id","เครื่องดื่ม","ท็อปปิ้ง","จำนวน","ราคา","จำนวนท็อปปิ้ง","ราคาท็อปปิ้ง","จำนวนเงิน"};
    
    @Override
    public int getRowCount() {
        return ProductList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
       ListProduct lp = ProductList.get(rowIndex);
       switch(columnIndex){
           case 0: return lp.getOrderId();
           case 1: return lp.getDrink();
           case 2: return lp.getTopping();
           case 3: return lp.getAmount();
           case 4: return lp.getPrice();
           case 5: return lp.getAmountTopping();
           case 6: return lp.getPriceTopping();
           case 7: return lp.getPrice()+lp.getPriceTopping();
       }
       return "";
    }
    
    public void setData(ArrayList<ListProduct> ProductList){
        this.ProductList = ProductList;
        fireTableDataChanged();
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }
}
