/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chala;

/**
 *
 * @author informatics
 */




import database.Vendor;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

public class VendorTableModel extends AbstractTableModel{

    ArrayList<Vendor> vendor = new ArrayList<Vendor>();
    String[] columnNames = {"id", "ชื่อบริษัท", "เบอร์โทร", "ที่อยู่", "ประเภทร้าน"};
    
    @Override
     public int getRowCount() {
        return vendor.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Vendor lp = vendor.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return lp.getVendorId();
            case 1:
                return lp.getName();
            case 2:
                return lp.getPhone();
            case 3:
                return lp.getAddress();
            case 4:
                return lp.getType();
        }
        return "";
    }
    
    public void setData(ArrayList<Vendor> vendor) {
        this.vendor = vendor;
        fireTableDataChanged();
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }
    
}
