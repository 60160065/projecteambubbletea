/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import chala.CHALA;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 *
 * @author Teddybank
 */
public class StockDao {

    private static Object[] productId;

    public static ArrayList<Stock> getStock() {
        ArrayList<Stock> stock = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT *\n"
                    + "  FROM product";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Stock emp = toObject(rs);
                stock.add(emp);
            }
            Database.close();
            return stock;
        } catch (SQLException ex) {
            Logger.getLogger(CHALA.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

    public static Stock toObject(ResultSet rs) throws SQLException {
        Stock stock;
        stock = new Stock();
        stock.setProductId(rs.getInt("productId"));
        stock.setName(rs.getString("name"));
        stock.setStorehouse(rs.getNString("srorehouse"));
        stock.setAmount(rs.getString("amount"));
        stock.setPrice(rs.getDouble("price"));
        stock.setTotal(rs.getDouble("rotal"));
        
        return stock;
    }

    public static Stock getStock(int StockID) {
        String sql = "SELECT * FROM employee WHERE empId = %d";
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, StockID));
            if(rs.next()){
                Stock stock = toObject(rs);
                Database.close();
                return stock;
            }
        } catch (SQLException ex) {
            Logger.getLogger(StockDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
    return null;
    }
}
