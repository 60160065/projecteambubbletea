/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import chala.CHALA;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Windows 10
 */
public class OrderingDao {

    public static ArrayList<ListProduct> getProduct() {
        ArrayList<ListProduct> lp = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT orderingId,\n"
                    + "       dataTime,\n"
                    + "       empId,\n"
                    + "       orderDetail.*\n"
                    + "  FROM ordering,orderDetail\n"
                    + " WHERE orderDetail = 1 AND \n"
                    + "       orderingId = 1 AND \n"
                    + "       empId = 1;";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                ListProduct list = toObject(rs);
                lp.add(list);
            }
            Database.close();
            return lp;
        } catch (SQLException ex) {
            Logger.getLogger(CHALA.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

    private static ListProduct toObject(ResultSet rs) throws SQLException {
        ListProduct list;
        list = new ListProduct();
        list.setOrderId(rs.getInt("orderId"));
        list.setDrink(rs.getString("drink"));
        list.setTopping(rs.getString("topping"));
        list.setAmount(rs.getInt("amount"));
        list.setPrice(rs.getDouble("price"));
        list.setAmountTopping(rs.getInt("amountTopping"));
        list.setPriceTopping(rs.getDouble("priceTopping"));
        list.setTotal(rs.getDouble("total"));
        return list;
    }

    public static ListProduct getProductList(int orderId) {
        String sql = "SELECT orderingId,\n"
                + "       orderDetail.*\n"
                + "  FROM ordering,orderDetail\n"
                + " WHERE  orderId = %d;";
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, orderId));
            if (rs.next()) {
                ListProduct listProduct = toObject(rs);
                Database.close();
                return listProduct;
            }
        } catch (SQLException ex) {
            Logger.getLogger(OrderingDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;

    }

    public static boolean insert(ListProduct listProduct) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO orderDetail (\n"
                    + "                            drink,\n"
                    + "                            topping,\n"
                    + "                            amount,\n"
                    + "                            price,\n"
                    + "                            amountTopping,\n"
                    + "                            priceTopping\n"
                    + "                        )\n"
                    + "                        VALUES ("
                    + "                            '%s',"
                    + "                            '%s',"
                    + "                            %d,"
                    + "                            %f,"
                    + "                            %d,"
                    + "                            %f"
                    + "                        );";
            System.out.println(String.format(sql,  listProduct.getDrink(), listProduct.getTopping(), listProduct.getAmount(), listProduct.getPrice(), listProduct.getAmountTopping(), listProduct.getPriceTopping()));
            stm.execute(String.format(sql,  listProduct.getDrink(), listProduct.getTopping(), listProduct.getAmount(), listProduct.getPrice(), listProduct.getAmountTopping(), listProduct.getPriceTopping()));
            Database.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(OrderingDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return true;
    }

    public static boolean update(ListProduct listProduct) {
        String sql = "UPDATE orderDetail\n"
                + "   SET drink = '%s',\n"
                + "       topping = '%s',\n"
                + "       amount = '%d',\n"
                + "       price = '%f',\n"
                + "       amountTopping = '%d',\n"
                + "       priceTopping = '%f',\n"
                + "       total = '%f'\n"
                + " WHERE orderId = '%d';";
        Connection conn = Database.connect();

        try {
            Statement stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql, listProduct.getDrink(), listProduct.getTopping(), listProduct.getAmount(), listProduct.getPrice(), listProduct.getAmountTopping(), listProduct.getPriceTopping(), listProduct.getTotal(), listProduct.getOrderId()));
            Database.close();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(OrderingDao.class.getName()).log(Level.SEVERE, null, ex);
        }

        Database.close();
        return false;
    }

    public static boolean delete(ListProduct listProduct) {
        String sql = "DELETE FROM orderDetail WHERE orderId = %d;";
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            boolean ret = stm.execute(String.format(sql, listProduct.getOrderId()));
            Database.close();
            return ret;
        } catch (SQLException ex) {
            Logger.getLogger(OrderingDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return true;
    }

    public static void save(ListProduct listProduct) {
        if (listProduct.getOrderId() < 0) {
            insert(listProduct);
        } else {
            update(listProduct);
        }
    }

}
