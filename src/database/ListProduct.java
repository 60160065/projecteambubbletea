/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author Windows 10
 */
public class ListProduct {

    int orderId;
    String drink;
    String topping;
    int amount;
    double price;
    int amountTopping;
    double priceTopping;
    double total;

    public ListProduct() {
        this.orderId = -1;
    }

    public ListProduct(int orderId, String drink, String topping, int amount, double price, int amountTopping, double priceTopping, double total) {
        this.orderId = orderId;
        this.drink = drink;
        this.topping = topping;
        this.amount = amount;
        this.price = price;
        this.amountTopping = amountTopping;
        this.priceTopping = priceTopping;
        this.total = total;
    }



    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getDrink() {
        return drink;
    }

    public void setDrink(String drink) {
        this.drink = drink;
    }

    public String getTopping() {
        return topping;
    }

    public void setTopping(String topping) {
        this.topping = topping;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getAmountTopping() {
        return amountTopping;
    }

    public void setAmountTopping(int amountTopping) {
        this.amountTopping = amountTopping;
    }

    public double getPriceTopping() {
        return priceTopping;
    }

    public void setPriceTopping(double priceTopping) {
        this.priceTopping = priceTopping;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "ListProduct{" + "orderId=" + orderId + ", drink=" + drink + ", topping=" + topping + ", amount=" + amount + ", price=" + price + ", amountTopping=" + amountTopping + ", priceTopping=" + priceTopping + ", total=" + total + '}';
    }



}
