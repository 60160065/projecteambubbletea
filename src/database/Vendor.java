/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author TAR1
 */
public class Vendor {
   int vendorId;
   String name;
   String phone;
   String address;
   String type;

    public Vendor() {
        this.vendorId = -1;
    }
   
    public Vendor(int vendorId, String name, String phone, String address, String type) {
        this.vendorId = vendorId;
        this.name = name;
        this.phone = phone;
        this.address = address;
        this.type = type;
    }


    public int getVendorId() {
        return vendorId;
    }

    public void setVendorId(int vendorId) {
        this.vendorId = vendorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
   
    @Override
    public String toString() {
        return "Vendor{" + "vendorId=" + vendorId + ", name=" + name + ", phone=" + phone + ", address=" + address + ", type=" + type + '}';
    }
   
}
