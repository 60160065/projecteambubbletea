/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import chala.CHALA;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author informatics
 */
public class VendorDao {
     private static Object[] vendorId;

    public static ArrayList<Vendor> getVen() {
        ArrayList<Vendor> vendor = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT *\n"
                    + "  FROM vendor";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Vendor ven = toObject(rs);
                vendor.add(ven);
            }
            Database.close();
            return vendor;
        } catch (SQLException ex) {
            Logger.getLogger(CHALA.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

    public static Vendor toObject(ResultSet rs) throws SQLException {
        Vendor vendor;
        vendor = new Vendor();
        vendor.setVendorId(rs.getInt("vendorId"));
        vendor.setName(rs.getString("name"));
        vendor.setPhone(rs.getString("phone"));
        vendor.setAddress(rs.getString("address"));
        vendor.setType(rs.getString("type"));
        return vendor;
    }

    public static Vendor getVendor(int venId) {
        String sql = "SELECT * FROM vendor WHERE vendorId = %d";
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, venId));
            if(rs.next()){
                Vendor ven = toObject(rs);
                Database.close();
                return ven;
            }
        } catch (SQLException ex) {
            Logger.getLogger(VendorDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
    return null;
    }
}
