/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import chala.CHALA;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 *
 * @author Windows 10
 */
public class EmployeeDao {

    private static Object[] empId;

    public static ArrayList<Employees> getEmp() {
        ArrayList<Employees> employees = new ArrayList();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT *\n"
                    + "  FROM employee";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                Employees emp = toObject(rs);
                employees.add(emp);
            }
            Database.close();
            return employees;
        } catch (SQLException ex) {
            Logger.getLogger(CHALA.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

    public static Employees toObject(ResultSet rs) throws SQLException {
        Employees employees;
        employees = new Employees();
        employees.setEmpId(rs.getInt("empId"));
        employees.setName(rs.getString("name"));
        employees.setSurname(rs.getString("surname"));
        employees.setPhone(rs.getString("phone"));
        employees.setCardId(rs.getString("cardId"));
        employees.setType(rs.getInt("type"));
        employees.setUsername(rs.getString("username"));
        employees.setPassword(rs.getString("password"));
        return employees;
    }

    public static Employees getEmployee(int empId) {
        String sql = "SELECT * FROM employee WHERE empId = %d";
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, empId));
            if(rs.next()){
                Employees employee = toObject(rs);
                Database.close();
                return employee;
            }
        } catch (SQLException ex) {
            Logger.getLogger(EmployeeDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
    return null;
    }
}
